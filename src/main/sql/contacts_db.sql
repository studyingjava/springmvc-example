DROP TABLE IF EXISTS contacts;
CREATE TABLE IF NOT EXISTS contacts (
  contact_id         INT(11) UNSIGNED AUTO_INCREMENT,
  first_name VARCHAR(50),
  last_name  VARCHAR(50),
  email      VARCHAR(50),
  telephone  VARCHAR(10),
  PRIMARY KEY (contact_id)
);
